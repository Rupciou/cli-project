#pragma once

namespace CppWinForm2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ Tytul;
	private: System::Windows::Forms::Button^ Start;
	private: System::Windows::Forms::Label^ ask1;
	private: System::Windows::Forms::Label^ ask2;
	private: System::Windows::Forms::Label^ ask3;
	private: System::Windows::Forms::Label^ ask4;
	private: System::Windows::Forms::Label^ ask5;
	private: System::Windows::Forms::Label^ ans1;
	private: System::Windows::Forms::Label^ ans2;
	private: System::Windows::Forms::Label^ ans3;
	private: System::Windows::Forms::Label^ ans4;
	private: System::Windows::Forms::Label^ ans5;
	private: int pyt=0;
	private: int punkty=0;
	private: System::Windows::Forms::Label^ wynik;
	private: array<System::String^, 2> ^Questions = gcnew array<System::String^, 2>(5, 4); //1Pytanie,2Prawda,3Fa連z
	private: array<System::String^> ^answersPlayer = gcnew array<System::String^>(5);//Odp gracz
	private: array<System::String^> ^answersTrue = gcnew array<System::String^>(5);//Odp prawidlowe
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->Tytul = (gcnew System::Windows::Forms::Label());
			this->Start = (gcnew System::Windows::Forms::Button());
			this->ask1 = (gcnew System::Windows::Forms::Label());
			this->ask2 = (gcnew System::Windows::Forms::Label());
			this->ask3 = (gcnew System::Windows::Forms::Label());
			this->ask4 = (gcnew System::Windows::Forms::Label());
			this->ask5 = (gcnew System::Windows::Forms::Label());
			this->ans1 = (gcnew System::Windows::Forms::Label());
			this->ans2 = (gcnew System::Windows::Forms::Label());
			this->ans3 = (gcnew System::Windows::Forms::Label());
			this->ans4 = (gcnew System::Windows::Forms::Label());
			this->ans5 = (gcnew System::Windows::Forms::Label());
			this->wynik = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// Tytul
			// 
			this->Tytul->AutoSize = true;
			this->Tytul->Font = (gcnew System::Drawing::Font(L"Arial", 14, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Tytul->Location = System::Drawing::Point(12, 26);
			this->Tytul->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Tytul->Name = L"Tytul";
			this->Tytul->Size = System::Drawing::Size(203, 22);
			this->Tytul->TabIndex = 0;
			this->Tytul->Text = L"Kolokwium z analizy!";
			this->Tytul->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Start
			// 
			this->Start->Location = System::Drawing::Point(97, 156);
			this->Start->Margin = System::Windows::Forms::Padding(2, 4, 2, 4);
			this->Start->Name = L"Start";
			this->Start->Size = System::Drawing::Size(90, 65);
			this->Start->TabIndex = 1;
			this->Start->Text = L"Pisz kolokwium!";
			this->Start->UseVisualStyleBackColor = true;
			this->Start->Click += gcnew System::EventHandler(this, &MyForm::Start_Click);
			// 
			// ask1
			// 
			this->ask1->AutoSize = true;
			this->ask1->Location = System::Drawing::Point(11, 79);
			this->ask1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ask1->Name = L"ask1";
			this->ask1->Size = System::Drawing::Size(34, 13);
			this->ask1->TabIndex = 2;
			this->ask1->Text = L"Pyt. 1";
			// 
			// ask2
			// 
			this->ask2->AutoSize = true;
			this->ask2->Location = System::Drawing::Point(66, 79);
			this->ask2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ask2->Name = L"ask2";
			this->ask2->Size = System::Drawing::Size(34, 13);
			this->ask2->TabIndex = 3;
			this->ask2->Text = L"Pyt. 2";
			// 
			// ask3
			// 
			this->ask3->AutoSize = true;
			this->ask3->Location = System::Drawing::Point(122, 79);
			this->ask3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ask3->Name = L"ask3";
			this->ask3->Size = System::Drawing::Size(34, 13);
			this->ask3->TabIndex = 4;
			this->ask3->Text = L"Pyt. 3";
			// 
			// ask4
			// 
			this->ask4->AutoSize = true;
			this->ask4->Location = System::Drawing::Point(181, 79);
			this->ask4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ask4->Name = L"ask4";
			this->ask4->Size = System::Drawing::Size(34, 13);
			this->ask4->TabIndex = 5;
			this->ask4->Text = L"Pyt. 4";
			// 
			// ask5
			// 
			this->ask5->AutoSize = true;
			this->ask5->Location = System::Drawing::Point(236, 79);
			this->ask5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ask5->Name = L"ask5";
			this->ask5->Size = System::Drawing::Size(31, 13);
			this->ask5->TabIndex = 6;
			this->ask5->Text = L"Pyt.5";
			// 
			// ans1
			// 
			this->ans1->AutoSize = true;
			this->ans1->Location = System::Drawing::Point(14, 115);
			this->ans1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ans1->Name = L"ans1";
			this->ans1->Size = System::Drawing::Size(31, 13);
			this->ans1->TabIndex = 7;
			this->ans1->Text = L"";
			// 
			// ans2
			// 
			this->ans2->AutoSize = true;
			this->ans2->Location = System::Drawing::Point(66, 115);
			this->ans2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ans2->Name = L"ans2";
			this->ans2->Size = System::Drawing::Size(31, 13);
			this->ans2->TabIndex = 8;
			this->ans2->Text = L"";
			// 
			// ans3
			// 
			this->ans3->AutoSize = true;
			this->ans3->Location = System::Drawing::Point(122, 115);
			this->ans3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ans3->Name = L"ans3";
			this->ans3->Size = System::Drawing::Size(31, 13);
			this->ans3->TabIndex = 9;
			this->ans3->Text = L"";
			// 
			// ans4
			// 
			this->ans4->AutoSize = true;
			this->ans4->Location = System::Drawing::Point(181, 115);
			this->ans4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ans4->Name = L"ans4";
			this->ans4->Size = System::Drawing::Size(31, 13);
			this->ans4->TabIndex = 10;
			this->ans4->Text = L"";
			// 
			// ans5
			// 
			this->ans5->AutoSize = true;
			this->ans5->Location = System::Drawing::Point(236, 115);
			this->ans5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->ans5->Name = L"ans5";
			this->ans5->Size = System::Drawing::Size(31, 13);
			this->ans5->TabIndex = 11;
			this->ans5->Text = L"";
			// 
			// wynik
			// 
			this->wynik->Location = System::Drawing::Point(0, 0);
			this->wynik->Name = L"wynik";
			this->wynik->Size = System::Drawing::Size(100, 23);
			this->wynik->TabIndex = 0;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(288, 262);
			this->Controls->Add(this->Tytul);
			this->Controls->Add(this->Start);
			this->Controls->Add(this->ask1);
			this->Controls->Add(this->ask2);
			this->Controls->Add(this->ask3);
			this->Controls->Add(this->ask4);
			this->Controls->Add(this->ask5);
			this->Controls->Add(this->ans1);
			this->Controls->Add(this->ans2);
			this->Controls->Add(this->ans3);
			this->Controls->Add(this->ans4);
			this->Controls->Add(this->ans5);
			this->Margin = System::Windows::Forms::Padding(2, 4, 2, 4);
			this->Name = L"MyForm";
			this->Text = L"Kolokwium v1.0";
			this->ResumeLayout(false);
			this->PerformLayout();
			
		}
#pragma endregion
	private: System::Void Start_Click(System::Object^  sender, System::EventArgs^  e) {
		Questions[0, 1] = "poch(x)=1";
		Questions[0, 2] = "Prawda";
		Questions[0, 3] = "Falsz";
		

		Questions[1, 1] = "poch(log(x))=1/x";
		Questions[1, 2] = "Prawda";
		Questions[1, 3] = "Fa連z";
		

		Questions[2, 1] = "Ca趾a(x)dx=2x^2+C";
		Questions[2, 2] = "Prawda";
		Questions[2, 3] = "Fa連z";
		

		Questions[3, 1] = "Ca趾a(xe^x)dx=x^2e^x+C";
		Questions[3, 2] = "Prawda";
		Questions[3, 3] = "Fa連z";
		

		Questions[4, 1] = "Ca趾a(sinx)dx=cosx+C";
		Questions[4, 2] = "Prawda";
		Questions[4, 3] = "Fa連z";


		answersTrue[0] = "Prawda";
		answersTrue[1] = "Prawda";
		answersTrue[2] = "Fa連z";
		answersTrue[3] = "Fa連z";
		answersTrue[4] = "Fa連z";


		if (Start->Text != "Zda貫�!" || Start->Text != "Uwali貫�!")
		{
			while (pyt < 5) {
				Form^ Kolo = gcnew Form();
				System::Windows::Forms::Label^ tresc = gcnew Label();
				System::Windows::Forms::RadioButton^ odpTrue = gcnew RadioButton();
				System::Windows::Forms::RadioButton^ odpFalse = gcnew RadioButton();
				System::Windows::Forms::Button^ Next = gcnew Button();
				System::Windows::Forms::GroupBox^ GrupBox1 = gcnew GroupBox();
				

				tresc->AutoSize = true;
				tresc->Location = System::Drawing::Point(40, 28);
				tresc->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
					static_cast<System::Byte>(238)));
				tresc->Size = System::Drawing::Size(179, 20);
				tresc->Name = L"tresc";

				odpTrue->AutoSize = true;
				odpTrue->Name = L"odpTrue";
				odpTrue->Location = System::Drawing::Point(17, 80);
				odpTrue->Size = System::Drawing::Size(85, 17);
				odpTrue->UseVisualStyleBackColor = true;
				odpTrue->Text = "";

				odpFalse->AutoSize = true;
				odpFalse->Name = L"odpFalse";
				odpFalse->Location = System::Drawing::Point(17, 100);
				odpFalse->Size = System::Drawing::Size(85, 17);
				odpFalse->UseVisualStyleBackColor = true;
				odpFalse->Text = "";

				Next->Location = System::Drawing::Point(500, 200);
				Next->Size = System::Drawing::Size(114, 29);
				Next->Name = L"Next";
				Next->Text = L"Nast瘼ne pytanie!";
				Next->UseVisualStyleBackColor = true;
				Next->DialogResult = System::Windows::Forms::DialogResult::OK;
				


				GrupBox1->Controls->Add(odpTrue);
				GrupBox1->Controls->Add(odpFalse);
				GrupBox1->Location = System::Drawing::Point(10000,10000);
				GrupBox1->Name = L"GrupBox1";
				GrupBox1->Size = System::Drawing::Size(398, 155);
				GrupBox1->Text = L"Wybieraj!:";
				GrupBox1->ResumeLayout(false);
				GrupBox1->PerformLayout();
				GrupBox1->SuspendLayout();
				

				Kolo->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				Kolo->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				Kolo->ClientSize = System::Drawing::Size(650, 250);
				Kolo->Name = L"Kolo";
				Kolo->Text = L"Pytanie" + (pyt + 1).ToString();
				Kolo->Controls->Add(GrupBox1);
				Kolo->Controls->Add(tresc);
				Kolo->Controls->Add(Next);
				Kolo->Controls->Add(odpFalse);
				Kolo->Controls->Add(odpTrue);
				Kolo->ResumeLayout(false);
				Kolo->PerformLayout();

				tresc->Text = Questions[pyt, 1];
				odpTrue->Text = Questions[pyt, 2];
				odpFalse->Text = Questions[pyt, 3];

				Kolo->ShowDialog();

				if (odpTrue->Checked) {
					answersPlayer[pyt] = "Prawda";
				}
				if (odpFalse->Checked) {
					answersPlayer[pyt] = "Fa連z";
				}

		/*		for (int i = pyt + 1; i < 5; i++) {
					answersPlayer[i] = "Brak";
				}*/

				pyt++;

				punkty = 0;

			
				if (answersPlayer[0] == answersTrue[0]) {
					punkty++;
					ans1->Text = "Dobrze!";
				}
				if (answersPlayer[1] == answersTrue[1]) {
					punkty++;
					ans2->Text = "Dobrze!";
				}
				if (answersPlayer[2] == answersTrue[2]) {
					punkty++;
					ans3->Text = "Dobrze!";
				}
				if (answersPlayer[3] == answersTrue[3]) {
					punkty++;
					ans4->Text = "Dobrze!";
				}
				if (answersPlayer[4] == answersTrue[4]) {
					punkty++;
					ans5->Text = "Dobrze!";
				}
				if (answersPlayer[0] != answersTrue[0]) {
					ans1->Text = "�le!";
				}
				if (answersPlayer[1] != answersTrue[1]) {
					ans2->Text = "�le!";
				}
				if (answersPlayer[2] != answersTrue[2]) {
					ans3->Text = "�le!";
				}
				if (answersPlayer[3] != answersTrue[3]) {
					ans4->Text = "�le!";
				}
				if (answersPlayer[4] != answersTrue[4]) {
					ans5->Text = "�le!";
				}

				
			}
		
			if (punkty >= 3) {
				Start->Text = "Zda貫�!";
			}
			else {
				Start->Text = "Uwali貫�!";
			}
		}
	
	}
};
}
